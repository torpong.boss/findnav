const axios = require('axios');
const cheerio = require('cheerio');
const _ = require('lodash')

var myArgs = process.argv.slice(2);

axios.request({
  url: "https://codequiz.azurewebsites.net/",
  method: "get",
  headers:{
    Cookie: "hasCookie=true;"
  } 
})
  .then(function (response) {
    const $ = cheerio.load(response.data)
    const result = $("body > table > tbody > tr").map((index, element) => ({
      fund: $(element).find('td:nth-of-type(1)').text().trim(),
      NAV: $(element).find('td:nth-of-type(2)').text().trim()
    })).get();

    const Fund = _.find(result, function(o) { return o.fund == myArgs[0]; });

    console.log(_.get(Fund, 'NAV', 'Not Found'))
  })
